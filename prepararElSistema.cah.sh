#!${BASH}
initialPath=$PWD

nameOfRepo="codedashuman-v0.1.5"
branch="main"

if [ -f "${nameOfRepo[@]}/bash/layCAH.sh" ]
  then
    echo "File ${nameOfRepo[@]}/bash/layCAH.sh already exist..."
  else
    mkdir ${nameOfRepo[@]}
    echo "Download repo ${nameOfRepo[@]}..."
    wget https://gitlab.com/elferrer/codedashuman/-/archive/${branch}/${nameOfRepo[@]}.tar.gz |
    echo -n "Extracting..."
    tar -xz -f "${nameOfRepo[@]}.tar.gz" -C ${nameOfRepo[@]} --strip-components=1
    rm "${nameOfRepo[@]}.tar.gz"
    echo "done."
fi

cd ${nameOfRepo[@]}
source "bash/layCAH.sh"
cd ..




titleOfManagementProject --title "Preparar el sistema"

header --describe "Mostrar efi boot"

explanatoryText "Escriba la contraseña para ver las entradas EFI."

su - root -c 'efibootmgr'

explanatoryText "Si realiza cambios con 'efibootmgr' debe actualizar el grub con 'update-grub'."



header --describe "Agregar derechos de administrador al usuario"

explanatoryText "Escriba la contraseña para agregar el usuario actual al grupo 'sudo'."
usuario=$USER
su - root -c "gpasswd -a ${usuario} sudo"

sudoersFile='/etc/sudoers'
explanatoryText "Escriba la contraseña para leer el archivo ${sudoersFile}."
sudoersChanges=$(su - root -c "grep -e '${usuario}' '${sudoersFile}' ")

if [ "$sudoersChanges" == "" ]
    then
        explanatoryText "Escriba la contraseña para agregar el usuario actual al archivo /etc/sudoers."
        su - root -c "echo '${usuario}  ALL=(ALL) NOPASSWD:ALL' >> ${sudoersFile}"
fi



header --describe "Instalar programas"

header --context "Eliminar cdrom de sources.list"

regexToSearch="^deb cdrom:"
fileToUpdate="/etc/apt/sources.list"
tempFile="/tmp/sources.list.temp"

explanatoryText "Escriba la contraseña para leer el archivo ${fileToUpdate}."

activedSource=$(grep -e "$regexToSearch" "$fileToUpdate")

if [ "$activedSource" != "" ]
    then
        explanatoryText "Escriba la contraseña para realizar cambios en el archivo ${fileToUpdate}."
        grep -v -e "$regexToSearch" "$fileToUpdate" > "$tempFile"
        echo -e "\n# Deactived sources" >> "$tempFile"
        echo "# $activedSource" >> "$tempFile"
        sudo mv $fileToUpdate "${fileToUpdate}.backup"
        sudo mv $tempFile $fileToUpdate
fi

testSource=$(grep -e "$regexToSearch" "$fileToUpdate")
activedSource=$?
weHope --firstOperator "${activedSource}" --sentence "!=" --secondOperator "0"



header --context "Agregar non-free sources.list"

sourceToSearch="non-free"
fileToUpdate="/etc/apt/sources.list"
testSource=$(grep -e "$sourceToSearch" "$fileToUpdate")
existSource=$?
if [ "$existSource" != "0" ]
    then
        sudo apt-add-repository $sourceToSearch
fi

testSource=$(grep -e "$sourceToSearch" "$fileToUpdate")
existSource=$?
weHope --firstOperator "$existSource" --sentence "==" --secondOperator "0"



header --context "Actualizar sistema"

sudo apt update -y
isUpdated=$?
weHope --firstOperator "$isUpdated" --sentence "==" --secondOperator "0"

sudo apt upgrade -y
isUpdated=$?
weHope --firstOperator "$isUpdated" --sentence "==" --secondOperator "0"



header --context "Instalar wifi"

ensureTool --program firmware-iwlwifi
sudo ln -s /sbin/modprobe /usr/bin/modprobe
sudo modprobe -r iwlwifi
sudo modprobe iwlwifi

lspci -nnk | grep 0280
isInstalled=$?

weHope --firstOperator "$isInstalled" --sentence "==" --secondOperator "0"



header --context "Instalar básicos"

ensureTool --program software-properties-common
ensureTool --program apt-transport-https
ensureTool --program net-tools
ensureTool --program exfat-utils
ensureTool --program curl



header --context "Instalar synaptic"
ensureTool --program synaptic
ensureTool --program apt-xapian-index


header --context "Instalar thunderbird"
ensureTool --program thunderbird



header --context "Instalar git"
ensureTool --program git
ensureTool --program git-lfs
git config --global push.default simple
git config --global core.editor nano
git config --global color.ui true
git config --global format.pretty oneline
printf "\nE-mail to git? "; read MAIL_GIT
git config --global user.email $MAIL_GIT
printf "\nLong name to git? "; read MAIL_NAME
git config --global user.name "$MAIL_NAME"



header --context "Instalar git-cola"
ensureTool --program git-cola



header --context "Instalar atom"

wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main"
sudo apt update -y
sudo apt install atom -y
sudo apt install myspell-es -y



header --context "Instalar ruby"

cook --recipe ruby



header --section "Instalar las gemas de ruby"
sudo rm -r ~/.bundle
ensureTool --gem "bundle"
ensureTool --gem "rake"
ensureTool --gem "rspec"



header --context "Instalar inkscape"

ensureTool --program inkscape




header --context "Instalar gimp"

ensureTool --program gimp
ensureTool --program gimp-data-extras
ensureTool --program gimp-gmic
ensureTool --program gimp-gutenprint
ensureTool --program gimp-ufraw
ensureTool --program gimp-lensfun
ensureTool --program gimp-plugin-registry




header --context "Instalar kdenlive"

ensureTool --program kdenlive



programToInstall="telegram"
header --context "Instalar ${programToInstall}"

checkInstalledProgram=$(isTheToolInstalled "Program" ${programToInstall})
isInstalled=$?
if [ "${isInstalled}" != 0 ]
  then
    wget https://telegram.org/dl/desktop/linux -O telegram.tar
    sudo tar -xJvf telegram.tar -C /opt
    sudo ln -sf /opt/Telegram/Telegram /usr/bin/telegram
    rm telegram.tar
fi

weHope --firstOperator $(which ${programToInstall}) --sentence "contains" --secondOperator ${programToInstall}



header --context "Limpiar sistema"

sudo apt autoremove -y
isUpdated=$?
weHope --firstOperator "$isUpdated" --sentence "==" --secondOperator "0"





cd $initialPath

header --describe "Ver los resultados"
logsFolder="${initialPath}/logs"
ensureManagementPanel --name $logsFolder
launchSpecificationsSummary --save "${logsFolder}/comprobarSistema"

header --context "Limpiar la memoria"
cleanProjectResultsFromMemory

return $(convertNegatedBooleanToCodeResponse ${theRunbookIsBrokenIs})
