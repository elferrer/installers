# To do

## Battery (lenovo)

sudo apt-get install tlp tlp-rdw tp-smapi-dkms acpi-call-dkms smartmontools
sudo tlp start
sudo tlp ac

###configure
sudo nano /etc/default/tlp

###inspect
sudo tlp stat -h
sudo tlp stat -p
sudo tlp stat -b


##Use tags in git projects

### Create a tag
git tag 18.01.12 5a8a -m 'Tag annotation'

### push tag
git push origin --tags

### show tag
git tag -l -n4 --points-at HEAD

### Use tag in docker to view last tag with five lines
docker-compose.yml :
    command: sh -c 'sleep 4 && echo "\n" && git tag -l -n5 && echo "\n" '

## Install Signal (signal.org):

curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
sudo apt update && sudo apt install signal-desktop

# create a livecd

sudo dd bs=4M if=neon-developer-20190708-2359.iso of=/dev/sdb conv=fdatasync
