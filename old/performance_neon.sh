#!/bin/bash

VAR=$1

case $VAR in
help)
printf "\nActions:\n"

printf "\n[System preparation:] "
printf "\n0. Update and clean system "
printf "\n1. Install intel microcode "
printf "\n2. Install tlp (Thinkpad Battery) "
printf "\n3. Fix servers to '/etc/dhcp/dhclient.conf' "
printf "\n4. Copy .bashrc to home "
printf "\n5. install synaptic "
printf "\n6. Install git "

printf "\n\n[Various programs:] "
printf "\n7. Install various system packages "
printf "\n8. Install various for web "
printf "\n9. Install atom "
printf "\n10. Install syncthing "
printf "\n11. Install Slack "
printf "\n12. Install chromium "
printf "\n13. Install postman "
printf "\n14. Install telegram "

printf "\n\n[Virtualization:] "
printf "\n15. Install kvm and libvirt virtualization "
printf "\n16. You would view existent virtual machines? "
printf "\n17. Install docker and docker-compose "

printf "\n\n[Other ruby installations:] "
printf "\n18. Install Ruby 2.4.0 "
printf "\n19. Install Rvm "
printf "\n20. Install Ruby240 with Rvm "

printf "\n\n[Android development:] "
printf "\n21. Install npm, further with npm install nodejs, cordova & ionic with -g parameter "
printf "\n22. Download Android SDK "
printf "\n23. Uncompress Android SDK "
printf "\n24. Configure paths for Android SDK "
printf "\n25. Download sdkman for gradle "
printf "\n26. Install gradle for ionic "

printf "\n\n[Artist jobs:] "
printf "\n27. Install texlive-full and kile "
printf "\n28. Install gimp, inkscape, darktable, kdenlive, digikam, kipi-plugins, luminance-hdr, pandora"
printf "\n29. Install oStoryBook "
printf "\n30. Install pencil (svg)"
printf "\n31. Install pencil2d (svg)"
printf "\n32. Install scribus, masterpdfeditor"

printf "\n\n[Utilities:] "
printf "\n33. Install slimbook utilities "

printf "\n\n[Continuous Integration:] "
printf "\n34. Install ansible "
printf "\n35. Install vagrant and plugin for libvirt "
printf "\n\n"
;;

0)
update_system() {
sudo pkcon refresh -y
sudo pkcon update -y
echo "End of action.\n"
}
printf "\nUpdate and clean system? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  update_system
fi
;;


1)
install_intelmicrocode() {
sudo pkcon install -y intel-microcode
echo "End of action.\n"
}
printf "\nInstall intel microcode? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_intelmicrocode
fi
;;


2)
install_tlp() {
sudo add-apt-repository ppa:linrunner/tlp
sudo pkcon refresh -y
sudo pkcon install -y tlp tlp-rdw tp-smapi-dkms acpi-call-dkms smartmontools pm-utils linux-tools tpb thinkfan acpitool
echo "End of action.\n"
}
printf "\nInstall tlp? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_tlp
fi
;;


3)
fix_dhcp() {
#echo 'supersede domain-name "fugue.com home.vix.com";' | sudo tee --append /etc/dhcp/dhclient.conf
echo 'prepend domain-name-servers 8.8.8.8 127.0.0.1;' | sudo tee --append /etc/dhcp/dhclient.conf
echo 'timeout 6;' | sudo tee --append /etc/dhcp/dhclient.conf
echo 'retry 10;' | sudo tee --append /etc/dhcp/dhclient.conf
echo "End of action.\n"
}
printf "\nFix servers to '/etc/dhcp/dhclient.conf'? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  fix_dhcp
fi
;;


4)
copy_bashrc() {
cp bashrc ~/.bashrc
echo "End of action.\n"
}
printf "\nCopy .bashrc to home? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  copy_bashrc
fi
;;


5)
install_synaptic() {
sudo pkcon install -y synaptic apt-xapian-index
#sudo update-apt-xapian-index -vf
sudo pkcon refresh -y
echo "End of action.\n"
}
printf "\ninstall synaptic? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_synaptic
fi
;;


6)
install_git() {

sudo pkcon install git-daemon-sysvinit git git-email git-lfs
printf "\nE-mail to git? "; read MAIL_GIT
git config --global user.email $MAIL_GIT
git config --global push.default simple
git config --global core.editor nano
git config --global diff.tool meld
git lfs install
printf "\nTo define long name to git, use the command:"
printf "\n\ngit config --global user.name 'Your complete name with spaces' \n\n"
echo "End of action.\n"
}
printf "\nInstall git? (y/n) "; read GIT
if [ $GIT != 'n' ]
  then
  install_git
fi
;;


7)
install_some_system() {
sudo pkcon -y install tidy meld curl build-essential patch zlib1g-dev \
	liblzma-dev libxslt-dev libxml2-dev exfat-utils
echo "End of action.\n"
}
echo "\nPackages: tidy meld curl build-essential patch zlib1g-dev
liblzma-dev libxslt-dev libxml2-dev exfat-utils"
printf "Install these packages? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_some_system
fi
;;


8)
install_some_web() {
sudo pkcon refresh -y
sudo pkcon update -y
sudo pkcon -y install epiphany-browser kmail
echo "End of action.\n"
}
echo "\nPackages: epiphany-browser kmail"
printf "Install these packages? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_some_web
fi
;;


9)
install_atom() {
wget -q https://packagecloud.io/AtomEditor/atom/gpgkey -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main"
sudo pkcon refresh -y
sudo pkcon update -y
sudo pkcon -y install atom software-properties-common apt-transport-https wget
echo "\n... if you would install eslint... you need first npm, and then:\n"
echo "apm install linter-eslint"
echo "\n\ninstall with npm:\n"
echo "npm i -g eslint [eslint-plugins]"
echo "\n...and activate Use Global Eslint package option\n\n"
echo "End of action.\n"
}
printf "\nInstall atom? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_atom
fi
;;


10)
install_syncthing() {
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
echo "deb https://apt.syncthing.net/ syncthing release" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo add-apt-repository -y ppa:nilarimogard/webupd8
sudo pkcon refresh -y
sudo pkcon install -y syncthing syncthing-gtk
echo "End of action.\n"
}
printf "\nInstall syncthing? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_syncthing
fi
;;


11)
install_slack() {
sudo pkcon install gconf2 gvfs-bin libgnome-keyring0 gir1.2-gnomekeyring-1.0 libappindicator1 -y
wget https://downloads.slack-edge.com/linux_releases/slack-desktop-3.0.2-amd64.deb
sudo dpkg -i slack-desktop-3.0.2-amd64.deb
rm slack-desktop-3.0.2-amd64.deb
echo "End of action.\n"
}
printf "\nInstall Slack v3.0.2? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_slack
fi
;;


12)
install_chromium() {
sudo pkcon install chromium-browser -y
echo "End of action.\n"
}
printf "\nInstall chromium? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_chromium
fi
;;


13)
install_postman() {
wget https://dl.pstmn.io/download/latest/linux64 -O postman.tar.gz
sudo tar -xzf postman.tar.gz -C /opt
rm postman.tar.gz
sudo ln -s /opt/Postman/Postman /usr/bin/postman
echo "End of action.\n"
}
printf "\nInstall postman? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_postman
fi
;;


14)
install_telegram() {
wget https://telegram.org/dl/desktop/linux -O telegram.tar
sudo tar -xJvf telegram.tar -C /opt
sudo ln -sf /opt/Telegram/Telegram /usr/bin/telegram
rm telegram.tar
echo "End of action.\n"
}
printf "\nInstall telegram? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_telegram
fi
;;


15)
install_kvm() {
sudo pkcon install -y qemu-kvm bridge-utils virt-manager libvirt-bin libvirt-dev qemu-utils qemu libosinfo-bin imvirt
sudo /etc/init.d/libvirt-bin restart
sudo usermod -aG libvirt $USER
sudo usermod -aG libvirt-qemu $USER
echo "\nPlease, for end installation you need reboot system.\n"
}
printf "\nInstall kvm and libvirt virtualization? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_kvm
fi
;;


16)
view_machines() {
virsh -c qemu:///system list
}
printf "\nYou would view existent virtual machines? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  view_machines
fi
;;


17)
install_docker() {

sudo pkcon install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo pkcon refresh -y
sudo pkcon install -y docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker
curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` > docker-compose
sudo mv docker-compose /usr/local/bin/.
sudo chmod +x /usr/local/bin/docker-compose
echo "End of action.\n"
}
printf "\nInstall docker and docker-compose? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_docker
fi
;;


18)
install_ruby260() {
sudo apt-add-repository ppa:brightbox/ruby-ng
sudo pkcon refresh -y
sudo pkcon install -y ruby2.6 ruby2.6-dev
sudo gem install bundle
sudo gem install rake
sudo gem install rspec
echo "End of action.\n"
}
printf "\nInstall Ruby 2.6.0 from new repositories? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_ruby260
fi
;;


19)
rvm_prepare() {
sudo apt-add-repository -y ppa:rael-gc/rvm
sudo pkcon refresh -y
sudo pkcon install -y rvm
cd ~
source /etc/profile.d/rvm.sh
echo rvm_autoupdate_flag=2 >> ~/.rvmrc
echo "export rvm_max_time_flag=20" >> ~/.rvmrc
rvm get master
echo ". /etc/profile.d/rvm.sh" >> ~/.bashrc
echo "You need reboot system to use rvm install.\n"
}
printf "\nInstall Rvm? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  rvm_prepare
fi
;;



20)
install_rvm_ruby240() {
cd ~
export rvmsudo_secure_path=1
rvm install ruby-2.4.0
rvm use 2.4.0
rvm alias create default ruby-2.4.0
unset GEM_HOME
rvmsudo rvm get stable --auto-dotfiles
sudo gem install rspec
echo "End of action.\n"
}
printf "\nInstall Ruby 2.4.0 with Rvm? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_ruby240
fi
;;


21)
install_npm() {
sudo pkcon install -y npm nodejs
mkdir ~/.npm-global
npm config set prefix '~/.npm-global'
echo "\n# NPM" >> ~/.profile
echo "export PATH=~/.npm-global/bin:$PATH" >> ~/.profile
source ~/.profile
npm install -g n
sudo n stable
sudo npm i -g npm
npm install -g nodejs cordova ionic
echo "End of action.\n"
}
printf "\nInstall npm, further with npm install nodejs, cordova & ionic with -g parameter? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_npm
fi
;;


22)
download_sdk() {
sudo pkcon install -y libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386
echo "..."
echo "Sorry, download you manually from android sdk:"
echo "https://developer.android.com/studio/index.html"
echo "and continue..."
}
printf "\nDownload Android SDK? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  download_sdk
fi
;;


23)
uncompress_androidsdk() {
7z x ~/Descargas/android-studio-ide-162.4069837-linux.zip
sudo mv ~/Descargas/android-studio /opt/
echo "Android sdk are unziped and moved ..."
echo "... locate in /opt/android-studio/bin, before you need execute 'sh studio.sh'"
echo "    and indicate the work directori at '~/Android/Sdk' in custom installaton.\n"
}
printf "\nUncompress Android SDK? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  uncompres_androidsdk
fi
;;


24)
configure_androidsdk() {
export ANDROID_HOME=~/Android/Sdk  >> ~/.bashrc
export PATH=$ANDROID_HOME/platform-tools:$PATH  >> ~/.bashrc
export PATH=$ANDROID_HOME/tools:$PATH  >> ~/.bashrc
echo "End of action.\n"
}
printf "\nConfigure paths for Android SDK? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  configure_androidsdk
fi
;;


25)
download_sdkman() {
cd ~
sudo rm .sdkman -r
curl -s "https://get.sdkman.io" | bash
echo "Sdkman for gradle are downloaded,"
echo "now for install open new terminal and execute:"
echo 'source "$HOME/.sdkman/bin/sdkman-init.sh" '
echo "Further test version with 'sdk version'."
}
printf "\nDownload sdkman for gradle? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  download_sdkman
fi
;;


26)
install_gradle() {
echo "Please, in other terminal, launch 'sdk install gradle 4.0'."
}
printf "\nInstall gradle for ionic? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_gradle
fi
;;


27)
install_texlive() {
sudo add-apt-repository -y ppa:jonathonf/texlive-2016
sudo pkcon refresh -y
sudo pkcon update -y

sudo pkcon install -y texlive-full kile kile-doc kile-l10n python-pygments \
  perl-tk fontforge context-nonfree context-doc-nonfree asymptote dblatex kbibtex \
  latex2html lilypond auctex doc-base dot2tex debhelper texlive-lang-all dh-make \
  texinfo-doc-nonfree xindy libspreadsheet-parseexcel-perl python-fontforge \
  fontforge-extras fontforge-doc potrace autotrace
echo "End of action.\n"
}
printf "\nInstall texlive-full and kile (up to 2gb)? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_texlive
fi
;;


28)
install_artist() {
sudo add-apt-repository -y ppa:inkscape.dev/trunk
sudo add-apt-repository -y ppa:otto-kesselgulasch/gimp
sudo add-apt-repository -y ppa:pmjdebruijn/darktable-unstable
sudo pkcon refresh -y
sudo pkcon update -y

sudo pkcon -y install inkscape \
	darktable kdenlive digikam kipi-plugins \
	luminance-hdr gimp gimp-data-extras gimp-gmic \
	gimp-gutenprint gimp-lensfun gimp-plugin-registry gimp-ufraw pandora
echo "End of action.\n"
}
printf "\nInstall gimp, inkscape, darktable, kdenlive, digikam, kipi-plugins, luminance-hdr, pandora? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_artist
fi
;;


29)
install_ostorybook() {
wget http://download.tuxfamily.org/ostorybook/6.00.00/oStorybook-6.00.00.deb
sudo dpkg -i oStorybook-6.00.00.deb
rm oStorybook-6.00.00.deb
echo "End of action.\n"
}
printf "\nInstall oStoryBook v5.04.01? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_ostorybook
fi
;;


30)
install_pencil() {
wget https://pencil.evolus.vn/dl/V3.0.4/Pencil_3.0.4_amd64.deb
sudo dpkg -i Pencil_3.0.4_amd64.deb
rm Pencil_3.0.4_amd64.deb
echo "End of action.\n"
}
printf "\nInstall pencil? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_pencil
fi
;;


31)
install_pencil2d() {
sudo pkcon -y install pencil2d
echo "End of action.\n"
}
printf "\nInstall pencil2d? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_pencil2d
fi
;;


32)
install_scribus() {
wget https://code-industry.net/public/master-pdf-editor-4.3.89_qt5.amd64.deb
sudo dpkg -i master-pdf-editor-4.3.89_qt5.amd64.deb
rm master-pdf-editor-4.3.89_qt5.amd64.deb
sudo add-apt-repository -y ppa:scribus/ppa
sudo pkcon refresh -y
sudo pkcon update -y
sudo pkcon -y install scribus-trunk
echo "End of action.\n"
}
printf "\nInstall scribus, masterpdfeditor? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_scribus
fi
;;


33)
install_slimbook() {
sudo add-apt-repository -y ppa:slimbook/slimbook
sudo pkcon refresh -y
sudo pkcon update -y
sudo pkcon -y install slimbookessentials
echo "End of action.\n"
}
printf "\nInstall slimbook utilities? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_slimbook
fi
;;


34)
install_ansible() {
sudo pkcon refresh -y
sudo pkcon update -y
sudo pkcon -y install ansible
echo "End of action.\n"
}
printf "\nInstall Ansible? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_ansible
fi
;;


35)
install_vagrant() {
sudo pkcon refresh -y
sudo pkcon update -y

wget https://releases.hashicorp.com/vagrant/2.0.2/vagrant_2.0.2_x86_64.deb
sudo dpkg -i vagrant_2.0.2_x86_64.deb
vagrant plugin install vagrant-libvirt
vagrant plugin install vagrant-mutate
vagrant box add CumulusCommunity/cumulus-vx --provider=libvirt
echo "End of action.\n"
}
printf "\nInstall Vagrant and plugins for libvirt? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_vagrant
fi
;;



*)
printf "Use help to show commands.\n"
;;
esac
