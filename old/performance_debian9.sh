#!/bin/bash

VAR=$1

case $VAR in
help)
printf "\nActions:\n"

printf "\n[System preparation:] "
printf "\n0.* Update and clean system "
printf "\n1. "
printf "\n2.* Install tlp (Thinkpad Battery) "
printf "\n3.* Fix servers to '/etc/dhcp/dhclient.conf' "
printf "\n4.* Copy .bashrc to home "
printf "\n5.* install synaptic "
printf "\n6.* Install git "

printf "\n\n[Various programs:] "
printf "\n7.* Install various system packages "
printf "\n8.* Install various for web, java "
printf "\n9.* Install atom "
printf "\n10.* Install syncthing "
printf "\n11.* Install Slack "
printf "\n12.* Install chromium "
printf "\n13.* Install postman "
printf "\n14.* Install telegram "

printf "\n\n[Virtualization:] "
printf "\n17.* Install docker and docker-compose "

printf "\n\n[Artist jobs:] "
printf "\n27.* Install gimp, inkscape, kdenlive, digikam, kipi-plugins, luminance-hdr, vlc, scribus "
printf "\n28.* Install oStoryBook "
printf "\n29.* Master PDF"
printf "\n30.* Install texlive-full and kile "
printf "\n\n"
printf "\n29. (scribus-trunk, darktable, pandora, page-crunch, pdftk, qpdfview)"
;;

0)
update_system() {
echo 'deb http://ftp.es.debian.org/debian/ stretch-backports main contrib non-free' | sudo tee --append /etc/apt/sources.list
sudo pkcon refresh
sudo pkcon update
sudo apt-get install -fq
echo "End of action.\n"
}
printf "\nUpdate and clean system? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  update_system
fi
;;



2)
install_tlp() {
sudo apt-get install -yq tlp tlp-rdw tp-smapi-dkms acpi-call-dkms smartmontools pm-utils linux-tools tpb thinkfan acpitool
echo "End of action.\n"
}
printf "\nInstall tlp? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_tlp
fi
;;


3)
fix_dhcp() {
echo 'supersede domain-name "fugue.com home.vix.com";' | sudo tee --append /etc/dhcp/dhclient.conf
echo 'prepend domain-name-servers 8.8.8.8 127.0.0.1;' | sudo tee --append /etc/dhcp/dhclient.conf
echo 'timeout 6;' | sudo tee --append /etc/dhcp/dhclient.conf
echo 'retry 10;' | sudo tee --append /etc/dhcp/dhclient.conf
echo "End of action.\n"
}
printf "\nFix servers to '/etc/dhcp/dhclient.conf'? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  fix_dhcp
fi
;;


4)
copy_bashrc() {
cp bashrc ~/.bashrc
echo "End of action.\n"
}
printf "\nCopy .bashrc to home? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  copy_bashrc
fi
;;


5)
install_synaptic() {
sudo apt-get -yq install synaptic apt-xapian-index
#sudo update-apt-xapian-index -vf
echo "End of action.\n"
}
printf "\ninstall synaptic? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_synaptic
fi
;;


6)
install_git() {
sudo apt-get -yq install git-daemon-sysvinit git git-email
printf "\nE-mail to git? "; read MAIL_GIT
git config --global user.email $MAIL_GIT
git config --global push.default simple
git config --global core.editor nano
git config --global diff.tool meld
printf "\nTo define long name to git, use the command:"
printf "\n\ngit config --global user.name 'Your complete name with spaces' \n\n"
echo "End of action.\n"
}
printf "\nInstall git? (y/n) "; read GIT
if [ $GIT != 'n' ]
  then
  install_git
fi
;;


7)
install_some_system() {
sudo apt-get -yq install yakuake tidy meld curl build-essential patch zlib1g-dev \
	liblzma-dev libxslt1-dev libxml2-dev
echo "End of action.\n"
}
echo "\nPackages: yakuake tidy meld curl build-essential patch zlib1g-dev
liblzma-dev libxslt-dev libxml2-dev latte-dock"
printf "Install these packages? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_some_system
fi
;;


8)
install_some_web() {
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C2518248EEA14886
sudo apt-get update -y -q
sudo apt-get -yq --allow-unauthenticated install epiphany-browser filezilla thunderbird  oracle-java8-installer
echo "End of action.\n"
}
echo "\nPackages: epiphany-browser filezilla thunderbird oracle-java8-installer"
printf "Install these packages? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_some_web
fi
;;


9)
install_atom() {
sudo apt-get -yq install desktop-file-utils gvfs gvfs-common gconf2 gvfs-bin
wget https://atom.io/download/deb -O atom.deb
sudo dpkg -i atom.deb
rm atom.deb
echo "\n... if you would install eslint... you need first npm, and then:\n"
echo "apm install linter-eslint"
echo "\n\ninstall with npm:\n"
echo "npm i -g eslint [eslint-plugins]"
echo "\n...and activate Use Global Eslint package option\n\n"
echo "End of action.\n"
}
printf "\nInstall atom? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_atom
fi
;;


10)
install_syncthing() {
sudo apt-get install -yq syncthing python-dateutil \
        libappindicator3-1 gir1.2-appindicator3-0.1 python-notify gir1.2-rsvg-2.0 \
	libdbusmenu-glib4 libdbusmenu-gtk3-4 libindicator3-7
wget http://ppa.launchpad.net/nilarimogard/webupd8/ubuntu/pool/main/s/syncthing-gtk/syncthing-gtk_0.9.2.3-1~webupd8~wily0_all.deb
sudo dpkg -i syncthing-gtk_0.9.2.3-1~webupd8~wily0_all.deb
rm syncthing-gtk_0.9.2.3-1~webupd8~wily0_all.deb
echo "End of action.\n"
}
printf "\nInstall syncthing? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_syncthing
fi
;;


11)
install_slack() {
sudo apt-get install -yq gconf2 gvfs-bin libgnome-keyring0 gir1.2-gnomekeyring-1.0 libappindicator1 \
	apt-transport-https
wget https://downloads.slack-edge.com/linux_releases/slack-desktop-3.0.2-amd64.deb
sudo dpkg -i slack-desktop-3.0.2-amd64.deb
rm slack-desktop-3.0.2-amd64.deb
echo "End of action.\n"
}
printf "\nInstall Slack v3.0.2? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_slack
fi
;;


12)
install_chromium() {
sudo apt-get install chromium -y -q
echo "End of action.\n"
}
printf "\nInstall chromium? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_chromium
fi
;;


13)
install_postman() {
wget https://dl.pstmn.io/download/latest/linux64 -O postman.tar.gz
sudo tar -xzf postman.tar.gz -C /opt
rm postman.tar.gz
sudo ln -s /opt/Postman/Postman /usr/bin/postman
echo "End of action.\n"
}
printf "\nInstall postman? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_postman
fi
;;


14)
install_telegram() {
wget https://telegram.org/dl/desktop/linux -O telegram.tar
sudo tar -xJvf telegram.tar -C /opt
sudo ln -sf /opt/Telegram/Telegram /usr/bin/telegram
rm telegram.tar
echo "End of action.\n"
}
printf "\nInstall telegram? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_telegram
fi
;;



17)
install_docker() {
sudo apt-get -yq install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"
sudo apt-get update -y -q
sudo apt-get install -y -q docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker
curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` > docker-compose
sudo mv docker-compose /usr/local/bin/.
sudo chmod +x /usr/local/bin/docker-compose
echo "End of action.\n"
}
printf "\nInstall docker and docker-compose? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_docker
fi
;;



27)
install_artist() {
sudo add-apt-repository -y ppa:inkscape.dev/trunk
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9DA4BD18B9A06DE3
sudo apt-get  -yq install inkscape 
sudo apt-get update -y -q
sudo apt-get upgrade -y -q
sudo apt-get -f install -y -q
sudo apt-get  -yq --allow-unauthenticated install kdenlive digikam kipi-plugins \
	luminance-hdr gimp gimp-data-extras gimp-gmic vlc \
	gimp-gutenprint gimp-lensfun gimp-plugin-registry gimp-ufraw \
	scribus scribus-template scribus-doc create-resources icc-profiles
echo "End of action.\n"
}
printf "\nInstall gimp, inkscape, kdenlive, digikam, kipi-plugins, luminance-hdr? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_artist
fi
;;


28)
install_ostorybook() {
wget http://download.tuxfamily.org/ostorybook/5.01.00/oStorybook-5.01.00.deb
sudo dpkg -i oStorybook-5.01.00.deb
rm oStorybook-5.01.00.deb
echo "End of action.\n"
}
printf "\nInstall oStoryBook v5.01? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_ostorybook
fi
;;


29)
install_masterpdf() {
wget http://get.code-industry.net/public/master-pdf-editor-4.3.82_qt5.amd64.deb
sudo dpkg -i master-pdf-editor-4.3.82_qt5.amd64.deb
echo "End of action.\n"
}
printf "\nInstall Master PDF editor? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_masterpdf
fi
;;


30)
install_texlive() {
sudo apt-get install -y -q texlive-full kile kile-doc kile-l10n python-pygments \
  perl-tk fontforge context-nonfree context-doc-nonfree asymptote dblatex kbibtex \
  latex2html auctex doc-base dot2tex debhelper texlive-lang-all dh-make \
  texinfo-doc-nonfree libspreadsheet-parseexcel-perl python-fontforge \
  fontforge-extras potrace
echo "End of action.\n"
}
printf "\nInstall texlive-full and kile (up to 2gb)? (y/n) "; read INTRO
if [ $INTRO != 'n' ]
  then
  install_texlive
fi
;;


*)
printf "Use help to show commands.\n"
;;
esac
